FROM nginx:stable

COPY ./dist/build/h5 /usr/share/nginx/html

COPY ./default.conf.template /etc/nginx/templates/default.conf.template

COPY ./nginx.conf /etc/nginx/nginx.conf

ENV TZ=Asia/Shanghai
