module.exports = {
  transpileDependencies: ['uview-ui'],
  devServer: {
    https: false,
    port: 8080,
    proxy: {
      '/api/qiyuesuo': {
        target: 'http://xura2e.natappfree.cc',
        changeOrigin: true,
        secure: false,
        pathRewrite: { '^/api/qiyuesuo': '' },
      },
      '/api/his': {
        target: 'http://192.168.230.11:5000',
        changeOrigin: false,
        secure: false,
        pathRewrite: { '^/api/his': '/api' },
      },
    },
  },
};
