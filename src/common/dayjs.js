import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';

dayjs.locale('zh-cn');

const install = (Vue, vm) => {
  vm.$u.dayjs = dayjs;
};

export default {
  install,
};
