const { decode } = require('js-base64');
const isBase64 = require('is-base64');

module.exports = {
  onLoad(query) {
    Object.keys(query).forEach((name) => {
      const val = query[name];
      query[name] = isBase64(val) ? decode(val) : val;
    });
  },
};
