git pull && docker pull nginx:stable

if [ -x "$(command -v docker-compose)" ]; then
  docker-compose down && docker-compose up -d --build
else
  docker compose down && docker compose up -d --build
fi
