# uni-app-template-v2

### 一个项目初始化之后，必须要做的事情

1. 安装 UI 框架 - uView
2. 封装网络请求库
3. 封装状态管理
4. 安装代码质量检查工具 - ESLint
5. 安装代码格式化工具 - Prettier
6. 安装 Git 消息格式化提交工具 - commitizen
7. 安装 Git 消息格式校验工具 - commitlint
8. 安装 Git 钩子方便管理程序 - husky
9. 安装 Git 暂存文件校验工具 - lint-staged

### docker-compose 部署

1. 拉取这个仓库到本地
2. 在根目录，创建`.env`文件

```
API_HIS_PROXY_PASS=http://gateway.docker.internal:5000/api
HOST_PORT=80
```
`API_HIS_PROXY_PASS`是`HIS`接口的地址，`HOST_PORT`是这个服务映射到主机的端口

3. 在`Linux`的终端，执行`sh docker-compose-deploy.sh`即可
