/* eslint-disable import/newline-after-import */
/* eslint-disable import/first */
import Vue from 'vue';
import uView from 'uview-ui';
import store from '@/store';
import App from './App';

Vue.config.productionTip = false;

App.mpType = 'app';

Vue.use(uView);

Vue.mixin(require('@/store/$u.mixin'));
Vue.mixin(require('@/common/decode-base64-query.mixin'));

const app = new Vue({
  store,
  ...App,
});

require('@/config/request.js')(app);
import httpApi from './config/api';
Vue.use(httpApi, app);

import dayjs from './common/dayjs';
Vue.use(dayjs, app);

app.$mount();
