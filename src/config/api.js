const { http } = uni.$u;

const install = (Vue, vm) => {
  /**
   * 获取配置
   */
  const getConfig = () =>
    http.post('/his/DB/DoFunc', {
      table: 'Hmis_Data.dbo.Config',
      action: 'select',
      field: 'item, val',
      where: "Section='契约锁'",
    });

  /**
   * 立即同步
   */
  const nowSync = (data) => http.get('/qiyuesuo/sync/now', data);

  vm.$u.api = {
    his: { getConfig },
    qiyuesuo: {
      nowSync,
    },
  };
};

export default {
  install,
};
